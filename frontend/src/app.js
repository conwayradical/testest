// Imports
import React from 'react';
import { Route, Switch } from 'react-router-dom'

// App Imports
import Layout from './components/layout'
import Tennis from './components/tennis';
import PageNotFound from './components/page-not-found';

const App  = () => (
    <Layout>
        <Switch>
            <Route exact path="/" component={ Tennis } />
            <Route component={ PageNotFound }/>
        </Switch>
    </Layout>
);

export default App;
