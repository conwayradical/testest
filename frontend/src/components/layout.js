// Imports
import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class Layout extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                { this.props.children }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        
    }
}

export default connect(mapStateToProps, {})(Layout);
