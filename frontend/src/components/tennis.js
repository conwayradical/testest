// Imports
import React, { Component } from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

const style = {
  margin: 12,
};

let player1Score = 'LOVE';
let player2Score = 'LOVE';
let playerThatWon = '';
let winner = false;
let Message;
let Reset;

class Tennis extends Component {

    constructor(props) {
        super(props);

        this.scoring = ['15','30','40','GAME'];
        this.state = {
            score1: 0,
            score2: 0
        };
    }
    
    render() {
        if(winner){
            Message = <div>Player {playerThatWon} won!</div>
            Reset = <RaisedButton onClick={this.onReset.bind(this)} label="Reset" primary={true} style={style} />
        }

        const CardExampleWithAvatar = () => (
          <Card>
            <CardHeader
              title="TES test"
            />
            <CardTitle title="Tennis Scorer" subtitle="does the job" />
            <CardText>
                <p>Player 1: {player1Score}</p>
                <p>Player 2: {player2Score}</p>
                {(this.state.score1 >= 4 || this.state.score2 >= 4) && Message}
            </CardText>
            <CardActions>
                <RaisedButton onClick={this.onItemClick.bind(this)} data-id="1" label="Player 1" primary={true} style={style} disabled={this.state.score1 >= 4 || this.state.score2 >= 4} />
                <RaisedButton onClick={this.onItemClick.bind(this)} data-id="2" label="Player 2" primary={true} style={style} disabled={this.state.score1 >= 4 || this.state.score2 >= 4} />
                {(this.state.score1 >= 4 || this.state.score2 >= 4) && Reset}
            </CardActions>
          </Card>
        );

        return (
            <section>
                <CardExampleWithAvatar />
            </section>
        )
    }
    onItemClick(event) {

        if(event.currentTarget.dataset.id == 1) {
            this.setState((state) => ({ score1: state.score1 + 1 }));
            player1Score = this.scoring[this.state.score1];
            if(this.state.score1 > 2) {
                playerThatWon = 'Player1';
                winner = true;  

            } 
        } else {
            this.setState((state) => ({ score2: state.score2 + 1 }));
            player2Score = this.scoring[this.state.score2];
            if(this.state.score2 > 2) {
                playerThatWon = 'Player2';
                winner = true;  
            }
        }
    }
    onReset(event) {
        this.setState((state) => ({ score1: 0, score2: 0 }));
        player1Score = 'LOVE';
        player2Score = 'LOVE';
        winner = false;
    }
}

export default Tennis;